<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Clients;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends  Controller
{
    /**
     * @Route("/home/login", name="login")
     */
    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils){
        $errors = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render("iibank/login.html.twig", array('errors'=>$errors, 'lastUsername'=>$lastUsername));
    }
}