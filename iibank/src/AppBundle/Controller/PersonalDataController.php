<?php
/**
 * Created by PhpStorm.
 * User: Jarosław
 * Date: 08.01.2018
 * Time: 22:15
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Clients;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class PersonalDataController extends Controller
{
    /**
     * @Route("/account/personalData", name="personalData")
     */
    public function perstonalDataAction(Request $request){
        $user = $this->getUser();
        $email = $user->getEmail();
        $name = $user->getName();
        $surname = $user->getSurname ();
        return $this->render(":iibank:personalData.html.twig", array('email'=>$email,'name'=>$name, 'surname'=>$surname));
    }

    /**
     * @Route("/account/personalData/changeEmail",  name="changeEmail")
     */
    public function changeEmailAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $user=$this->getUser();
        $repository = $this->getDoctrine()->getRepository(Clients::class);
        $form = $this->createFormBuilder($user)
            ->add("email", EmailType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('save', SubmitType::class, array('label' =>"Zapisz", 'attr' => array('class' => 'btn btn-success text-uppercase text-white', 'style' => 'margin-bottom:15px')))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()) {
            $user = $form->getData();
            $tmp = $repository->findOneBy(array('email' => $user->getEmail()));
            if (!$tmp) {
                $em->persist($user);
                $em->flush();
                return $this->redirectToRoute("personalData");
            }
            else{
                return $this->render(":iibank:changeEmailExist.html.twig");
            }
        }
        return $this->render(":iibank:changeEmail.html.twig",array('form'=>$form->createView()));
    }

    /**
     * @Route("/account/personalData/changePassword", name="changePassword")
     */
    public function changePasswordAction(Request $request, UserPasswordEncoderInterface $encoder ){
        $em = $this->getDoctrine()->getManager();
        $user=$this->getUser();
        $form = $this->createFormBuilder($user)
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Źle powtórzone hasło.',
                'options' => array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')),
                'required' => true,
                'first_options'  => array('label' => 'Hasło: '),
                'second_options' => array('label' => 'Powtórz Hasło: ')))
            ->add('save', SubmitType::class, array('label' =>"Zapisz", 'attr' => array('class' => 'btn btn-success text-uppercase text-white', 'style' => 'margin-bottom:15px')))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()){
            $user = $form->getData();
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute("personalData");
        }
        return $this->render(":iibank:changePassword.html.twig",array('form'=>$form->createView()));
    }
}