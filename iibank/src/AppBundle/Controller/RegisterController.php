<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Clients;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class RegisterController extends Controller
{
    /**
     * @Route("/home/register", name="register")
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $encoder){
        $client = new Clients();
        $em = $this->getDoctrine()->getManager();
        $registerForm = $this->createFormBuilder($client)
            ->add('name', TextType::class, array('label' =>"Imię: ",'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('surname', TextType::class, array('label' =>"Nazwisko: ", 'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('email', EmailType::class, array('label' =>"Email: ", 'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Źle powtórzone hasło.',
                'options' => array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')),
                'required' => true,
                'first_options'  => array('label' => 'Hasło: '),
                'second_options' => array('label' => 'Powtórz Hasło: ')))
            ->add('save', SubmitType::class, array('label' =>"Zarejestruj", 'attr' => array('class' => 'btn btn-default text-uppercase btn-light text-dark', 'style' => 'margin-bottom:15px')))
            ->getForm();

        $registerForm->handleRequest($request);

        if($registerForm->isSubmitted()&&$registerForm->isValid()){
            $client = $registerForm->getData();
            $repository = $this->getDoctrine()->getRepository(Clients::class);
            $checkEmail = $repository->findOneBy(array('email' => $client->getEmail()));
            if (!$checkEmail) {
                do {
                    $client->setUsername(rand(1000, 9999));
                    $checkUsername = $repository->findOneBy(array('username' => $client->getUsername()));
                }while($checkUsername);
                do {
                    $part1 = rand(10, 99);
                    $part2 = rand(1000,9999);
                    $part3 = rand(1000,9999);;
                    $part4 = "0000";
                    $part5 = rand(1000,9999);;
                    $part6 = rand(1000,9999);;
                    $part7 = rand(1000,9999);;

                    $client->setAccountNumber($part1." ".$part2." ".$part3." ".$part4." ".$part5." ".$part6." ".$part7);
                    $checkAccountNumber=$repository->findOneBy(array('accountNumber' => $client->getAccountNumber()));
                }while($checkAccountNumber);

                $client->setPassword($encoder->encodePassword($client, $client->getPassword()));
                $client->setResources(10000.00);
                $em->persist($client);
                $em->flush();
                $id = $client->getIdClients();
                $stmt = "CREATE VIEW view".$id." AS SELECT transactions.date_of_transaction, transactions.title, transactions.sender_name, transactions.sender_number, transactions.receiver_name, transactions.receiver_number, transactions.amount FROM historylist INNER JOIN transactions ON historylist.transaction_id=transactions.id_transaction WHERE historylist.client_id='".$id."' ORDER BY transactions.date_of_transaction DESC";
                $em->getConnection()->executeQuery($stmt);
                $username = $client->getUsername();
                return $this->render("iibank/registerCompleted.html.twig", array('username' => $username));
            } else {
                return $this->render("iibank/registerEmailExist.html.twig");
            }
        }
        return $this->render("iibank/register.html.twig", array('registerForm'=>$registerForm->createView()));
    }
}