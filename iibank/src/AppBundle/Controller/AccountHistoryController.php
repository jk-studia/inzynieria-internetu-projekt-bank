<?php

namespace AppBundle\Controller;

use AppBundle\Entity\HistoryList;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class AccountHistoryController extends Controller
{
    /**
     * @Route("/account/accountHistory", name="accountHistory")
     *
     */
    public function accountHistoryAction(Request $request){
        $repository = $this->getDoctrine()->getRepository(HistoryList::class);
        $user = $this->getUser();
        $accountNumber = $user->getAccountNumber();
        $id = $user->getIdClients();
        $em = $this->getDoctrine()->getManager();
        $stmt = "Select * from view".$id;
        $sql = $em->getConnection()->executeQuery($stmt);
        return $this->render(":iibank:accountHistory.html.twig", array("history"=>$sql));
    }


}