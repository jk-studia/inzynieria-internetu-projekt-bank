<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Clients;
use AppBundle\Entity\Contacts;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AccountContactsController extends Controller
{
    /**
     * @Route("/account/accountContacts", name="accountContacts")
     */
    public function contactAction(Request $request){
        $repository = $this->getDoctrine()->getRepository(Contacts::class);
        $user = $this->getUser();
        $userID = $user->getIdClients();
        $contacts = $repository->findBy(array('client_id'=>$userID));
        return $this->render(":iibank:accountContacts.html.twig", array("contacts"=>$contacts));
    }

    /**
     * @Route("/account/accountContacts/delete/{idContacts}", name="contactsDelete")
     */
    public function deleteContactAction(Request $request, $idContacts){
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Contacts::class);
        $contact = $repository->find($idContacts);
        $em->remove($contact);
        $em->flush();
        return $this->redirectToRoute("accountContacts");
    }

    /**
     * @Route("/account/accountContacts/create", name="contactsCreate")
     */
    public function createContactAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $contact = new Contacts();
        $userID = $this->getUser()->getIdClients();
        $addContactForm = $this->createFormBuilder($contact)
            ->add('contactName', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('personalName', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('contactNumber', TextType::class, array('invalid_message'=>'Złe dane', 'attr' => array('class' => 'form-control',  'style' => 'margin-bottom:15px')))
            ->add('save', SubmitType::class, array('label' =>"Zapisz", 'attr' => array('class' => 'btn btn-success text-uppercase text-white', 'style' => 'margin-bottom:15px')))
            ->getForm();
        $addContactForm->handleRequest($request);
        if($addContactForm->isSubmitted()&&$addContactForm->isValid()){
            $contact = $addContactForm->getData();
            $check = $this->checkPattern($contact->getContactNumber());
            if(!$check){
                return $this->render(":iibank:accountAddContactFailed.html.twig");
            }else {
                $contact->setClientId($userID);
                $em->persist($contact);
                $em->flush();
                return $this->redirectToRoute("accountContacts");
            }
        }
        return $this->render(":iibank:accountAddContact.html.twig", array('addContactForm'=>$addContactForm->createView()));
    }

    /**
     * @Route("/account/accountContacts/search", name="contactsSearch")
     */
    public function searchContactAction(Request $request){
        $newUser = new Clients();
        $searchUser = new Clients();
        $searchContact = $this->createFormBuilder($searchUser)
            ->add('name', TextType::class, array('label'=>"Imię:", 'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('surname', TextType::class, array('label'=>"Nazwisko:", 'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('search', SubmitType::class, array('label' =>"Szukaj", 'attr' => array('class' => 'btn btn-success text-uppercase text-white', 'style' => 'margin-bottom:15px')))
            ->getForm();
        $searchContact->handleRequest($request);
        if($searchContact->isSubmitted()&&$searchContact->isValid()){
            $searchUser = $searchContact->getData();
            $repository = $this->getDoctrine()->getRepository(Clients::class);
            $newUser = $repository->findBy(array('name'=>$searchUser->getName(),'surname'=>$searchUser->getSurname()));
            if($newUser){
                return $this->render(":iibank:contactfound.html.twig", array("newUser"=>$newUser));
            }
            else{
                return $this->render(":iibank:searchingContactFailed.html.twig");
            }
        }
        return $this->render(":iibank:accountContactSearch.html.twig", array('searchContact'=>$searchContact->createView()));
    }

    /**
     * @Route("/account/accountContacts/create/{name}/{surname}/{accountNumber}", name="contactsCreateFound")
     */
    public function createFoundContactAction(Request $request, $name, $surname, $accountNumber){
        $personalName = $name." ".$surname;
        $em = $this->getDoctrine()->getManager();
        $contact = new Contacts();
        $userID = $this->getUser()->getIdClients();
        $addContactForm = $this->createFormBuilder($contact)
            ->add('contactName', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('personalName', TextType::class, array('attr' => array('class' => 'form-control', 'value'=>$personalName, 'style' => 'margin-bottom:15px')))
            ->add('contactNumber', TextType::class, array('invalid_message'=>'Złe dane', 'attr' => array('class' => 'form-control', 'value'=>$accountNumber,  'style' => 'margin-bottom:15px')))
            ->add('save', SubmitType::class, array('label' =>"Zapisz", 'attr' => array('class' => 'btn btn-success text-uppercase text-white', 'style' => 'margin-bottom:15px')))
            ->getForm();
        $addContactForm->handleRequest($request);
        if($addContactForm->isSubmitted()&&$addContactForm->isValid()){
            $contact = $addContactForm->getData();
            $check = $this->checkPattern($contact->getContactNumber());
            if(!$check){
                return $this->render(":iibank:accountAddContactFailed.html.twig");
            }else {
                $contact->setClientId($userID);
                $em->persist($contact);
                $em->flush();
                return $this->redirectToRoute("accountContacts");
            }
        }
        return $this->render(":iibank:accountAddContact.html.twig", array('addContactForm'=>$addContactForm->createView()));
    }

    public function checkPattern($check){
        $pattern = "@^[0-9]{2} [0-9]{4} [0-9]{4} [0]{4} [0-9]{4} [0-9]{4} [0-9]{4}$@";
        $result = preg_match($pattern, $check);
        return $result;
    }
}