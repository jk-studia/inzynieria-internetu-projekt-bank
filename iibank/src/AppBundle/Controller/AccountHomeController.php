<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Clients;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AccountHomeController extends Controller
{
    /**
     * @Route("/account/accountHome", name="accountHome")
     *
     */
    public function accountHomeAction(Request $request){
        $user = $this->getUser();
        $accountNumber = $user->getAccountNumber();
        $resources = $user->getResources();
        return $this->render(":iibank:accountHome.html.twig", array('accountNumber'=>$accountNumber,'resources'=>$resources));
    }
}