<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Clients;
use AppBundle\Entity\HistoryList;
use AppBundle\Entity\Transactions;
use AppBundle\Entity\TransactionsOtherBank;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AccountTransactionController extends Controller
{
    /**
     * @Route("/account/accountTransaction", name="accountTransaction")
     */
    public function accountTransactionAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $transaction = new Transactions();

        $transfer = $this->createFormBuilder($transaction)
            ->add('receiverName', TextType::class, array('label'=>'Odbiorca: ','attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('title', TextType::class, array('label'=>'Tytuł: ', 'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('receiverNumber', TextType::class, array('label'=>'Numer Konta: ','invalid_message'=>'Złe dane', 'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('amount', MoneyType::class, array('label'=>'Kwota: ', 'invalid_message'=>'Złe dane', 'currency'=>'PLN','attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('save', SubmitType::class, array('label' =>"Wyślij przelew", 'attr' => array('class' => 'btn btn-default text-uppercase btn-light text-dark', 'style' => 'margin-bottom:15px')))
            ->getForm();
        $transfer->handleRequest($request);
        if($transfer->isSubmitted()&&$transfer->isValid()){
            $transaction=$transfer->getData();
            $transaction->setDateOfTransaction();
            if($transaction->getAmount()>$user->getResources()||$transaction->getAmount()<=0){
                $reason = "Za mało środków na koncie";
                return $this->render( ":iibank:accountTransferFailed.html.twig", array('reason'=>$reason));
            }
            else {
                $reason = "Źle wpisany numer konta";
                $check = $this->checkPattern($transaction->getReceiverNumber());
                if (!$check) {
                    return $this->render(":iibank:accountTransferFailed.html.twig", array('reason'=>$reason));
                } else {
                    $transaction->setSenderNumber($user->getAccountNumber());
                    $transaction->setSenderName($user->getName() . " " . $user->getSurname());
                    $em->persist($transaction);
                    $em->flush();
                    $user->setResources($user->getResources() - $transaction->getAmount());
                    $em->persist($user);
                    $em->flush();

                    $repositoryReceiver = $this->getDoctrine()->getRepository(Clients::class);
                    $history1 = new HistoryList();
                    $history2 = new HistoryList();
                    $receiver = $repositoryReceiver->findOneBy(array('accountNumber' => $transaction->getReceiverNumber()));
                    if ($receiver) {
                        $receiver->setResources($receiver->getResources() + $transaction->getAmount());
                        $em->persist($receiver);
                        $em->flush();
                        $history1->setClientId($user->getIdClients());
                        $history1->setTransactionId($transaction->getIdTransaction());
                        $em->persist($history1);
                        $em->flush();
                        $history2->setClientId($receiver->getIdClients());
                        $history2->setTransactionId($transaction->getIdTransaction());
                        $em->persist($history2);
                        $em->flush();
                    } else {
                        $history2->setClientId($user->getIdClients());
                        $history2->setTransactionId($transaction->getIdTransaction());
                        $em->persist($history2);
                        $em->flush();
                        $transactionOtherBank = new TransactionsOtherBank();
                        $transactionOtherBank->setDateOfTransaction();
                        $transactionOtherBank->setTitle($transaction->getTitle());
                        $transactionOtherBank->setSenderName($transaction->getSenderName());
                        $transactionOtherBank->setSenderNumber($transaction->getSenderNumber());
                        $transactionOtherBank->setReceiverName($transaction->getReceiverName());
                        $transactionOtherBank->setReceiverNumber($transaction->getReceiverNumber());
                        $transactionOtherBank->setAmount($transaction->getAmount());
                        $em->persist($transactionOtherBank);
                        $em->flush();
                    }

                    return $this->render(":iibank:accountTransferCompleted.html.twig");
                }
            }
        }
        return $this->render(":iibank:accountTransaction.html.twig", array("transfer"=>$transfer->createView()));
    }

    /**
     * @Route("/account/accountTransaction/{contactNumber}/{personalName}", name="accountTransactionContact")
     *
     */
    public function accountTransactionContactAction(Request $request, $contactNumber, $personalName){
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $transaction = new Transactions();
        $transfer = $this->createFormBuilder($transaction)
            ->add('receiverName', TextType::class, array('label'=>'Odbiorca: ','attr' => array('class' => 'form-control', 'value'=>$personalName, 'style' => 'margin-bottom:15px')))
            ->add('title', TextType::class, array('label'=>'Tytuł: ', 'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('receiverNumber', TextType::class, array('label'=>'Numer Konta: ', 'attr' => array('class' => 'form-control', 'value'=>$contactNumber, 'style' => 'margin-bottom:15px')))
            ->add('amount', MoneyType::class, array('label'=>'Kwota: ', 'currency'=>'PLN','attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('save', SubmitType::class, array('label' =>"Wyślij przelew", 'attr' => array('class' => 'btn btn-default text-uppercase btn-light text-dark', 'style' => 'margin-bottom:15px')))
            ->getForm();
        $transfer->handleRequest($request);
        if($transfer->isSubmitted()&&$transfer->isValid()){
            $transaction=$transfer->getData();
            if($transaction->getAmount()>$user->getResources()||$transaction->getAmount()<=0){
                $reason = "Za mało środków na koncie";
                return $this->render( ":iibank:accountTransferFailed.html.twig", array('reason'=>$reason));
            }
            else {
                $check = $this->checkPattern($transaction->getReceiverNumber());
                if (!$check) {
                    $reason = "Źle wpisany numer konta";
                    return $this->render(":iibank:accountTransferFailed.html.twig", array('reason'=>$reason));
                } else {
                    $transaction->setSenderNumber($user->getAccountNumber());
                    $transaction->setSenderName($user->getName() . " " . $user->getSurname());
                    $transaction->setDateOfTransaction();

                    $em->persist($transaction);
                    $em->flush();
                    $user->setResources($user->getResources() - $transaction->getAmount());
                    $em->persist($user);
                    $em->flush();

                    $repositoryReceiver = $this->getDoctrine()->getRepository(Clients::class);
                    $history1 = new HistoryList();
                    $history2 = new HistoryList();
                    $receiver = $repositoryReceiver->findOneBy(array('accountNumber' => $transaction->getReceiverNumber()));
                    if ($receiver) {
                        $receiver->setResources($receiver->getResources() + $transaction->getAmount());
                        $em->persist($receiver);
                        $em->flush();
                        $history1->setClientId($user->getIdClients());
                        $history1->setTransactionId($transaction->getIdTransaction());
                        $em->persist($history1);
                        $em->flush();
                        $history2->setClientId($receiver->getIdClients());
                        $history2->setTransactionId($transaction->getIdTransaction());
                        $em->persist($history2);
                        $em->flush();
                    } else {
                        $history2->setClientId($user->getIdClients());
                        $history2->setTransactionId($transaction->getIdTransaction());
                        $em->persist($history2);
                        $em->flush();
                        $transactionOtherBank = new TransactionsOtherBank();
                        $transactionOtherBank->setDateOfTransaction();
                        $transactionOtherBank->setTitle($transaction->getTitle());
                        $transactionOtherBank->setSenderName($transaction->getSenderName());
                        $transactionOtherBank->setSenderNumber($transaction->getSenderNumber());
                        $transactionOtherBank->setReceiverName($transaction->getReceiverName());
                        $transactionOtherBank->setReceiverNumber($transaction->getReceiverNumber());
                        $transactionOtherBank->setAmount($transaction->getAmount());
                        $em->persist($transactionOtherBank);
                        $em->flush();
                    }


                    return $this->render(":iibank:accountTransferCompleted.html.twig");
                }
            }
        }
        return $this->render(":iibank:accountTransaction.html.twig", array("transfer"=>$transfer->createView()));
    }

    public function checkPattern($check){
        $pattern = "@^[0-9]{2} [0-9]{4} [0-9]{4} [0]{4} [0-9]{4} [0-9]{4} [0-9]{4}$@";
        $result = preg_match($pattern, $check);
        return $result;
    }
}