<?php
/**
 * Created by PhpStorm.
 * User: Jarosław
 * Date: 10.12.2017
 * Time: 13:30
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class LogoutController extends Controller
{
    /**
     * @Route("/home/logout", name="logout")
     */
    public function logoutAction(Request $request){
    }
    /**
     * @Route("/home/returnPage", name="returnPage")
     */
    public function logoutInformationAction(Request $request){
        return $this->render(":iibank:logout.html.twig");
    }
}