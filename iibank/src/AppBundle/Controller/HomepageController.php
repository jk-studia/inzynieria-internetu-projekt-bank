<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HomepageController extends Controller
{

    /**
     * @Route("/", name="start")
     */
    public function startAction(Request $request){
       return $this->render("iibank/homepage.html.twig");
    }

    /**
     * @Route("/home/homepage", name="homepage")
     */
    public function homepageAction(Request $request){
        return $this->render("iibank/homepage.html.twig");
    }

    /**
     * @Route("/home/description", name="description")
     */
    public function descriptionAction(Request $request){
        return $this->render("iibank/description.html.twig");
    }

}