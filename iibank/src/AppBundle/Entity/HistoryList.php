<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="HistoryList")
 */
class HistoryList
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $historyID;

    /**
     * @ORM\Column(type="integer")
     * @ORM\ManyToOne(targetEntity="Clients")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="idClients")
     */
    private $client_id;

    /**
     * @ORM\Column(type="integer")
     * @ORM\ManyToOne(targetEntity="Transactions")
     * @ORM\JoinColumn(name="transaction_id", referencedColumnName="IdTransaction")
     */
    private $transaction_id;

    /**
     * @return mixed
     */
    public function getHistoryID()
    {
        return $this->historyID;
    }

    /**
     * @param mixed $historyID
     */
    public function setHistoryID($historyID)
    {
        $this->historyID = $historyID;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * @param mixed $client_id
     */
    public function setClientId($client_id)
    {
        $this->client_id = $client_id;
    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transaction_id;
    }

    /**
     * @param mixed $transaction_id
     */
    public function setTransactionId($transaction_id)
    {
        $this->transaction_id = $transaction_id;
    }


}