<?php
/**
 * Created by PhpStorm.
 * User: Jarosław
 * Date: 15.01.2018
 * Time: 05:56
 */

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="Investment")
 */
class Investment
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $investment;

    /**
     * @ORM\Column(type="integer")
     * @ORM\ManyToOne(targetEntity="Clients")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="idClients")
     */
    private $client_id;

    /**
     * @ORM\Column(type="datetime", name="date_of_invest")
     */
    private $dateOfInvest;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $funds;

    /**
     * @return mixed
     */
    public function getInvestment()
    {
        return $this->investment;
    }

    /**
     * @param mixed $investment
     */
    public function setInvestment($investment)
    {
        $this->investment = $investment;
    }



    /**
     * @return mixed
     */
    public function getDateOfInvest()
    {
        return $this->dateOfInvest;
    }

    /**
     * @param mixed $dateOfInvest
     */
    public function setDateOfInvest($dateOfInvest)
    {
        $this->dateOfInvest = $dateOfInvest;
    }

    /**
     * @return mixed
     */
    public function getFunds()
    {
        return $this->funds;
    }

    /**
     * @param mixed $funds
     */
    public function setFunds($funds)
    {
        $this->funds = $funds;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * @param mixed $client_id
     */
    public function setClientId($client_id)
    {
        $this->client_id = $client_id;
    }
}