<?php
/**
 * Created by PhpStorm.
 * User: Jarosław
 * Date: 15.01.2018
 * Time: 05:46
 */

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Credit")
 */
class Credit
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $IdCredit;

    /**
     * @ORM\Column(type="integer")
     * @ORM\OneToOne(targetEntity="Clients")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="idClients")
     */
    private $client_id;
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $debt;

    /**
     * @ORM\Column(type="datetime", name="date_of_borrow")
     */
    private $dateOfBorrow;

    /**
     * @ORM\Column(type="datetime", name="date_of_pay_up")
     */
    private $dateOfPayUp;

    /**
     * @return mixed
     */

    /**
     * @return mixed
     */
    public function getDebt()
    {
        return $this->debt;
    }

    /**
     * @param mixed $debt
     */
    public function setDebt($debt)
    {
        $this->debt = $debt;
    }

    /**
     * @return mixed
     */
    public function getDateOfPayUp()
    {
        return $this->dateOfPayUp;
    }

    /**
     * @param mixed $dateOfPayUp
     */
    public function setDateOfPayUp($dateOfPayUp)
    {
        $this->dateOfPayUp = $dateOfPayUp;
    }

    /**
     * @return mixed
     */
    public function getDateOfBorrow()
    {
        return $this->dateOfBorrow;
    }

    /**
     * @param mixed $dateOfBorrow
     */
    public function setDateOfBorrow($dateOfBorrow)
    {
        $this->$dateOfBorrow = new \DateTime("now");
    }

    /**
     * @return mixed
     */
    public function getIdCredit()
    {
        return $this->IdCredit;
    }

    /**
     * @param mixed $IdCredit
     */
    public function setIdCredit($IdCredit)
    {
        $this->IdCredit = $IdCredit;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * @param mixed $client_id
     */
    public function setClientId($client_id)
    {
        $this->client_id = $client_id;
    }
}