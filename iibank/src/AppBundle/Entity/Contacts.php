<?php
/**
 * Created by PhpStorm.
 * User: Jarosław
 * Date: 03.12.2017
 * Time: 01:48
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="Contacts")
 */
class Contacts
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idContact;

    /**
     * @Orm\Column(type="string", length=100)
     */
    private $contactName;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $personalName;

    /**
     * @ORM\Column(type="string", unique=true, length=100)
     */
    private $contactNumber;

    /**
     * @ORM\Column(type="integer")
     * @ORM\ManyToOne(targetEntity="Clients")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="idClients")
     */
    private $client_id;

    // ==============================================================  getters

    /**
     * @return mixed
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * @return mixed
     */
    public function getIdContact()
    {
        return $this->idContact;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * @return mixed
     */
    public function getContactNumber()
    {
        return $this->contactNumber;
    }


    // ======================================================================= setters


    /**
     * @param mixed $contactName
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;
    }

    /**
     * @param mixed $idContact
     */
    public function setIdContact($idContact)
    {
        $this->idContact = $idContact;
    }

    /**
     * @param mixed $client_id
     */
    public function setClientId($client_id)
    {
        $this->client_id = $client_id;
    }


    /**
     * @param mixed $contactNumber
     */
    public function setContactNumber($contactNumber)
    {
        $this->contactNumber = $contactNumber;
    }

    /**
     * @return mixed
     */
    public function getPersonalName()
    {
        return $this->personalName;
    }

    /**
     * @param mixed $personalName
     */
    public function setPersonalName($personalName)
    {
        $this->personalName = $personalName;
    }

}