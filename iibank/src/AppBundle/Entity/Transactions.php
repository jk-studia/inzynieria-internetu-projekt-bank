<?php
/**
 * Created by PhpStorm.
 * User: Jarosław
 * Date: 10.01.2018
 * Time: 20:58
 */

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Transactions")
 */
class Transactions
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $IdTransaction;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $senderName;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $senderNumber;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $receiverNumber;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $receiverName;

    /**
     * @ORM\Column(type="datetime", name="date_of_transaction")
     */
    private $dateOfTransaction;

    /**
     * @return mixed
     */
    public function getReceiverNumber()
    {
        return $this->receiverNumber;
    }

    /**
     * @param mixed $receiverNumber
     */
    public function setReceiverNumber($receiverNumber)
    {
        $this->receiverNumber = $receiverNumber;
    }

    /**
     * @return mixed
     */
    public function getSenderNumber()
    {
        return $this->senderNumber;
    }

    /**
     * @param mixed $senderNumber
     */
    public function setSenderNumber($senderNumber)
    {
        $this->senderNumber = $senderNumber;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getIdTransaction()
    {
        return $this->IdTransaction;
    }

    /**
     * @param mixed $IdTransaction
     */
    public function setIdTransaction($IdTransaction)
    {
        $this->IdTransaction = $IdTransaction;
    }

    /**
     * @return mixed
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * @param mixed $senderName
     */
    public function setSenderName($senderName)
    {
        $this->senderName = $senderName;
    }

    /**
     * @return mixed
     */
    public function getReceiverName()
    {
        return $this->receiverName;
    }

    /**
     * @param mixed $receiverName
     */
    public function setReceiverName($receiverName)
    {
        $this->receiverName = $receiverName;
    }

    /**
     * @return mixed
     */
    public function getDateOfTransaction()
    {
        return $this->dateOfTransaction;
    }

    /**
     * @param mixed $dateOfTransaction
     */
    public function setDateOfTransaction()
    {
        $this->dateOfTransaction = new \DateTime("now");
    }


}